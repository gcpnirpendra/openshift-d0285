# Openshift-D0285 
  -  **DO180**    Red Hat OpenShift I: Containers & Kubernetes
-    **DO280**    DO280 Red Hat OpenShift Administration
  

  ## DAY WISE TOPICS
  ### DAY 1

  - Introducing container technology
- Describe how software can run in containers orchestrated by Red Hat OpenShift Container Platform.
- Creating containerized services
- Provision a service using container technology.
- Managing containers
- Modify pre-build container images to create and manage containerized services.
- Managing container images
- Manage the life cycle of a container image from creation to deletion.
- Creating custom container images
- Design and code a Container file to build a custom container image.


### DAY 2
- Deploying containerized applications on OpenShift
- Deploy single container applications on OpenShift Container Platform.
- Deploying multi-container applications
- Deploy applications that are containerized using multiple container images.
- Troubleshooting containerized applications
- Troubleshoot a containerized application deployed on OpenShift.
- Comprehensive review of introduction to container, Kubernetes, and Red Hat OpenShift
- Demonstrate how to containerize a software application, test it with Podman, and deploy it on an OpenShift cluster.

### DAY 3
 
#### DO280 Red Hat OpenShift Administration
-  Describing Red Hat OpenShift Container Platform
-  Describing OpenShift Container Platform Features
-  Quiz: Describing OpenShift Container Platform Features
-  Describing OpenShift Container Platform Architecture
-  Quiz: Describing OpenShift Container Platform Architecture
-  Quiz: Describing Cluster Operators
-  Summary

#### Verifying a Cluster
 - Describing Installation Methods
-   Quiz: Describing Installation Methods
-  Configuring a Lab Environment
-  Guided Exercise: Configuring a Lab Environment
-  Executing Troubleshooting Commands
-  Guided Exercise: Executing Troubleshooting Commands
-  Summary



### DAY 4

#### Configuring Authentication
 - Configuring Identity Providers
- Guided Exercise: Configuring Identity Providers


#### Controlling Access to OpenShift Resources
 - Defining and Applying Permissions Using RBAC
-  Guided Exercise: Defining and Applying Permissions using RBAC

***Troubleshoot***

- Pod related issues
  - Router/Registry Not deploying to correct node
  - Registry not showing contents of NFS mount (persistent volume)
  - Hosts Can No Longer Resolve Each Other During Anisble Install
  - Failure to deploy registry (permissions issues)
  - Application Pod fails to deploy
- Issues with Nodes
  - Nodes being reported as ready, but builds failing
  - Node reporting NotReady
  - Nodes report ready but ETCD health check fails
  - Atomic-openshift-node service fails to start
- Registry issues
  - OpenShift builds fail trying to push image using a wrong IP address for the registry
  - OpenShift build error: failed to push image while using NFS persistent storage
  - Failure to push image to OpenShift’s Registry when backed by shared storage
- Quotas and Limitranges
  - Must make a non-zero request for cpu


#### Managing Sensitive Information with Secrets
 - Guided Exercise: Managing Sensitive Information With Secrets
-  Controlling Application Permissions with Security Context Constraints (SCCs)
-  Guided Exercise: Controlling Application Permissions with Security Context
-  Constraints (SCC)
-  Lab: Controlling access to OpenShift resources
- Summary

#### Configuring OpenShift Networking Components
-  Troubleshooting OpenShift Software-defined Networking
-  Guided Exercise: Troubleshooting OpenShift Software-defined Networking
-  Controlling Cluster Network Ingress
-  Guided Exercise: Controlling Cluster Network Ingress
-  Lab: Configuring OpenShift Networking Components
-  Summary

#### Controlling Pod Scheduling
 - Controlling Pod Scheduling Behavior
-  Guided Exercise: Controlling Pod Scheduling Behavior
-  Limiting Resource Usage
-  Guided Exercise: Limiting Resource Usage


### Day 5

#### Scaling an Application
 - Guided Exercise: Scaling an Application
-  Lab: Controlling Pod Scheduling
-  Summary

#### Scaling an OpenShift Cluster
 
-  Manually Scaling an OpenShift Cluster
-  Guided Exercise: Manually Scaling an OpenShift Cluster
-  Automatically Scaling an OpenShift Cluster
-  Guided Exercise: Automatically Scaling an OpenShift Cluster
-  Summary

#### Performing Cluster Updates
-  Describing the Cluster Update Process

#### OpenShift Backup and DR

- Installation and Configuration of Kasten/Velero
- Setup the backup of etcd
- Recovering from the etcd backup





### Day 6
#### Managing a Cluster with the Web Console
 - Performing Cluster Administration
-  Guided Exercise: Performing Cluster Administration
-  Managing Workloads
-  Guided Exercise: Managing Workloads
-  Examining Cluster Metrics
-  Guided Exercise: Examining Cluster Metrics
-  Lab: Managing the Cluster with the Web Console

#### Post_installation_configuration

- Configuration of Authentication with Htpasswd
- Configuration of Authentication with Azure AD
- Remove the default virtual admin user (kubeadmin)
- Secure Api with ssl certificate
- Secure Route with Route
- Setting the Ingress Controller
- Restricting the API server to private
- Configuration Default Quota project template
- Configure default limits
- Restrict user for LoadBalancer service.
- Configure Alert Manager
- Updating the global cluster pull secret
- Configure Autoscaling for nodes
- Create infrastracture nodes
- Move all infr related services to infra nodes
- OpenShift Internal Registry
- Router pods
- Monitoring pods
- Logging pods





